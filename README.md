# Видеоархив
Система для удобного, в контектсе представления данных как видео, хранения и предпросмотра пользовательских видео-исходников.
# Зависимости
Для развертывания системы требуется наличие:

- [Python](http://www.python.org/download) >= 2.7.6;
- [Django framework](https://www.djangoproject.com) == 1.7;
- Python [httplib2](http://code.google.com/p/httplib2) >= 2.0.8;
- Python: [Google apiclient, oauth2client](http://code.google.com/p/google-api-python-client/downloads/list) (*python-google-api-python-client_\*_all*)
# Ограничения
Накладываются третьмими сервисами на:  

1.  Размер одного файла видео __не более 64 ГБ__ (YouTube);
2.  Обязательная верификация учетной записи YouTube пользователя.
# Решение возможных проблем
>__Проблема__: При запуске сервера c Django, появляеться ошибка в трассировке стека:  
>`ssl.SSLError: [Errno 185090050] _ssl.c:340: error:0B084002:x509 certificate routines:X509_load_cert_crl_file:system lib`.
>
>__Решение__: Следует добавить права на выполнение для учетной записи из под которой работает сервер для файла _cacerts.txt_ из [httplib2](http://code.google.com/p/httplib2), например для текущего пользователя, командой:  
>`sudo chmod +x /usr/local/lib/python*.*/dist-packages/httplib*-*.*-py*.*.egg/httplib2/cacerts.txt`  
>* * *
>__Проблема__: Django ругается на недопутимую локаль в `first_name`/`last_name` в базе данных.
>
>__Решение__: По умолчанию модели в Django создаются в кодировке `latin7` локали `latin7_general_ci`, для кириллических имен и фамилий следует выполнить _SQL_-запросы в вашем СУБД клиенте (для СУБД _MySQL_):  
>   ALTER TABLE auth_user MODIFY COLUMN first_name VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;  
>   ALTER TABLE auth_user MODIFY COLUMN last_name VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
>* * *
>__Проблема__: При запуске тестового сервера появляются варнинги типа: `RenamedInDjango18Warning: ... 'queryset' method should be renamed 'get_queryset'`.
>
>__Решение__: Данное поведение Django (>=1.7) вызвано, что приложение 'django-guardian' использует метод `queryset()` который переименован в `get_queryset()`. Для совместимости со след. версиями Django рекомендуется изменить в  
>`/usr/local/lib/python2.7/dist-packages/guardian/admin.py` :  
>`def queryset(self, request):` на `def get_queryset(self, request):`, и добавить метод:  
>`def queryset(self, request):
    return get_queryset(self, request)`
