from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
#from django.contrib.staticfiles.urls import staticfiles_urlpatterns
#from django.conf import settings
#from django.conf.urls.static import static
#from django.views.generic.simple import direct_to_template
from django.views.generic import TemplateView
from base_site.owl_video import views


admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'base_site.views.home', name='home'),
    # url(r'^base_site/', include('base_site.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^comments/', include('django_comments.urls')), #copy templates from django.contrib.comments.templates
    #~ url(r'^comments/post/',  ),
    #~ url(r'^comments/', include('django.contrib.comments.urls')),
    
    url( r'^$', views.showIndex ),
    #~ url( r'^test/videos/$', direct_to_template, {'template' : 'flat_pages/videos.html'} ),
    #~ url( r'^test/video/$', direct_to_template, {'template' : 'flat_pages/video.html'} ),
    #~ url( r'^test/login/$', direct_to_template, {'template' : 'flat_pages/login.html'} ),
    #~ url( r'^test/create/$', direct_to_template, {'template' : 'flat_pages/register.html'} ),
    #~ url( r'^test/remind/$', direct_to_template, {'template' : 'flat_pages/pass_remind.html'} ),
    url( r'^test/videos/$', TemplateView.as_view( template_name = "flat_pages/videos.html" ) ),
    url( r'^test/video/$', TemplateView.as_view( template_name = "flat_pages/video.html" ) ),
    url( r'^test/video_panel/$', TemplateView.as_view( template_name = "flat_pages/video1.html" ) ),
    url( r'^test/login/$', TemplateView.as_view( template_name = "flat_pages/login.html" ) ),
    url( r'^test/create/$', TemplateView.as_view( template_name = "flat_pages/register.html" ) ),
    url( r'^test/remind/$', TemplateView.as_view( template_name = "flat_pages/pass_remind.html" ) ),
    
    url( r'^video/list/$', views.viewVideoList ),
    url( r'^video/(\d+)/$', views.viewVideoEntry ),
    url( r'^event/(\d+)/$', views.viewEventEntry ),
    url( r'^event/list/$', views.viewEventList ),
    #~ url( r'^view/user/$', views.viewVideoEntry ),
    #~ url( r'^view/event/$', views.viewVideoEntry ),
    #~ url( r'^event/list/$', views.viewVideoEntry ),
    
    url( r'^account/view/(\d+)/$', views.viewAccount ),
    url( r'^account/login/$', views.userLogin ),
    url( r'^account/logout/$', views.userLogout ),
    url( r'^account/create/$', views.userRegister ),
    url( r'^account/google_auth/$', views.userGrantAccessGoogle ),
    url( r'^account/google_callback/$', views.callBackHandlerFromGoogle ),
    #~ url( r'^test/account/$', direct_to_template, {'template' : 'flat_pages/account.html'} ),
    url( r'^test/account/$', TemplateView.as_view( template_name = "flat_pages/account.html" ) ),
    url( r'^test/email/$', TemplateView.as_view( template_name = "email.html" ) ),
    
    #~ url( r'^test/logged/$', direct_to_template, {'template' : 'htmls/log_head.html'} ),
    url( r'^test/logged/$', TemplateView.as_view( template_name = "htmls/log_head.html" ) ),
    url( r'^test/view/$', TemplateView.as_view( template_name = "base_anonym_account.html" ) ),
    url( r'^test/view-user/$', TemplateView.as_view( template_name = "flat_pages/account.html" ) ),
    url( r'^test/events/$', TemplateView.as_view( template_name = "flat_pages/events.html" ) ),
    url( r'^test/event/$', TemplateView.as_view( template_name = "flat_pages/event.html" ) ),
    url( r'^test/index/$', TemplateView.as_view( template_name = "flat_pages/index.html" ) ),
    url( r'^test/error404/$', TemplateView.as_view( template_name = "flat_pages/error_404.html" ) ),
    url( r'^test/error500/$', TemplateView.as_view( template_name = "flat_pages/error_500.html" ) ),
    url( r'^test/storage/$', TemplateView.as_view( template_name = "flat_pages/storage.html" ) ),
    url( r'^test/entry/$', TemplateView.as_view( template_name = "flat_pages/entry.html" ) ),
    url( r'^test/project/$', TemplateView.as_view( template_name = "flat_pages/project.html" ) ),
    url( r'^test/main/$', TemplateView.as_view( template_name = "flat_pages/main.html" ) ),
    
    url( r'^ajax/checkname/$', views.ajaxUserNameResolver ),
    url( r'^ajax/checkevent/$', views.ajaxEventNameResolver ),
    url( r'^appgetuser/$', views.sendLocalAppCreds ),
    url( r'^appreserve/$', views.reserveVideoId ),
    url( r'^appadddata/$', views.additionalVideoData ),
    url( r'^app_add_user_object/$', views.addUserArchiveObject ),
    url( r'^app_get_user_acc_places/$', views.getStorageProjectUserPerm ),
    url( r'^app_get_user_acc_archive/$', views.getUserAccessArchive )

    
    
)
# + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

#urlpatterns += staticfiles_urlpatterns()
