$(document).ready( function() {
    $("#id_userlogin").tooltip({ placement: "right", trigger: "focus hover", title: "Требуется заполнить, от 2 до 16 символов"});
    $("#id_name").tooltip({ placement: "right", trigger: "focus hover", title: "Опционально, если не указана Фамилия"});
    $("#id_surname").tooltip({ placement: "right", trigger: "focus hover", title: "Опционально, если не указано Имя"});
    $("#id_email").tooltip({ placement: "right", trigger: "focus hover", title: "Требуется заполнить"});
    $("#id_f_password").tooltip({ placement: "right", trigger: "focus hover", title: "Требуется заполнить, макс. 32 символа"});
    //$("#id_s_password").tooltip({ placement: "right", trigger: "focus hover", title: "Требуется заполнить, от 2 до 16 символов"});
    
    $("#closeDU").click( function() { $( this ).parent().fadeOut("slow"); });
    $("#closeDN").click( function() { $( this ).parent().fadeOut("slow"); });
    $("#closeDS").click( function() { $( this ).parent().fadeOut("slow"); });
    $("#closeDE").click( function() { $( this ).parent().fadeOut("slow"); });
    $("#closeDP").click( function() { $( this ).parent().fadeOut("slow");  });
    $("#closeDNS").click( function() { $( this ).parent().fadeOut("slow");  });
    $("#resetButt").click( function() { for ( var key in check_submit ) {
                if( (key == "name") || (key == "surname") ) check_submit[key] = true;
                else check_submit[key] = false;
            }
            submitActivateCheck( check_submit );
        }
    );
    $("#submitButt").prop("disabled", "disabled");
    
    var check_submit = {
        login : false,
        email: false,
        f_pass: false,
        s_pass: false,
        name: true,
        surname: true
    }
    
    function submitActivateCheck( submit_propertys ) {
        if ( Object.keys( submit_propertys ).every( function(key) { return submit_propertys[key]; } ) ) $("#submitButt").removeProp("disabled");
        else $("#submitButt").prop("disabled", "disabled");
        //for ( var key in submit_propertys ) console.log( key + " " + submit_propertys[key] + "\n" );
        //console.log( "================\n" );
    }
    
    
    
    function replaceEscapeChars( found ) {
        return "0x" + String ( found.charCodeAt(0) ) + "&#8203;";
    }
    
    $("#id_userlogin").blur(
        function( event ) {
            
            var pattern = /^[a-zA-Z0-9\+\.\-@_]{2,16}$/;
            var inputusername = $( this ).val();
            pattern.test( inputusername ) ? check_submit.login = true : check_submit.login = false;
            if ( ! check_submit.login ) {
                $("#js_Umess").empty();
                $("#js_Umess").append("<strong>&apos;" + inputusername.replace( /[^a-zA-Z0-9\+\.\-@_]/g, replaceEscapeChars) +
                "&apos;</strong> не допустимый <strong>Логин</strong>. Разрешена Латиница, цифры и знаки: <strong>@ + - _ .</strong>" );
                $("#closeDU").parent().fadeIn("slow");
            }
            else {
                
            
            
            
            //============================================AJAX_CHECK=============================================
                
                $.post( "/ajax/checkname/", { new_login : inputusername }, 
                    function( resp ) {
                        resp == "ok" ? check_submit.login = true : check_submit.login = false;
                        if ( ! check_submit.login ) {
                            $("#js_Umess").empty();
                            $("#js_Umess").append("&apos;<strong>" + inputusername + "</strong>&apos; уже существует" );
                            $("#closeDU").parent().fadeIn("slow");
                        }
                        else {
                            $("#closeDU").parent().fadeOut("slow");
                        }
                    }
                );
            //============================================AJAX_CHECK=============================================
            }
            submitActivateCheck( check_submit );
        }
    );
    
    $("#id_email").blur(
        function(event) {
            var pattern = /^[^\s\t\n\r#\$%&\?\^]+@[^\s\t\n\r#\$%&\?\^]+$/;
            var inputemail = $( this ).val();
            pattern.test( inputemail ) ? check_submit.email = true : check_submit.email = false;
            if ( ! check_submit.email ) {
                $("#js_Emess").empty();
                $("#js_Emess").append("<strong>&apos;" + inputemail.replace( /[^a-zA-Z0-9\+\.\-@_]/g, replaceEscapeChars ) + "&apos;</strong> не является адресом <strong>E-mail</strong>" );
                $("#closeDE").parent().fadeIn("slow");
            }
            else {
                $("#closeDE").parent().fadeOut("slow");
            }
            submitActivateCheck( check_submit );
        }
    );
    $("#id_f_password").blur(
        function(event) {
            var pattern = /^\w+$/
            var inputpass = $( this ).val();
            pattern.test( inputpass ) ? check_submit.f_pass = true : check_submit.f_pass = false;
            if ( ! check_submit.f_pass ) {
                $("#js_Pmess").empty();
                $("#js_Pmess").append("Пароль должен содержать только<br><strong>ASCII символы</strong>" );
                $("#closeDP").parent().fadeIn("slow");
            }
            else {
                $("#closeDP").parent().fadeOut("slow");
                $("#id_s_password").fadeIn("fast");
                
            }
            submitActivateCheck( check_submit );
        }
    );
    $("#id_s_password").blur(
        function(event) {
            var f_pass = $("#id_f_password").val();
            var s_pass = $("#id_s_password").val();
            f_pass === s_pass ? check_submit.s_pass = true : check_submit.s_pass = false;
            if ( ! check_submit.s_pass ) {
                $("#js_Pmess").empty();
                $("#js_Pmess").append("Введеные пароли <strong>не совпадают</strong>");
                $("#closeDP").parent().fadeIn("slow");
            }
            else {
                $("#closeDP").parent().fadeOut("slow");
                
            }
            submitActivateCheck( check_submit );
            
        }
    );
    $("#id_name").change(
        function(event) {
            var pattern = /^[a-zA-Z\u0400-\u04ff]+$/;
            var inputname = $( this ).val();
            var inputsname = $("#id_surname").val();
            
            
            if ( inputname ) {
                
                pattern.test( inputname ) ? check_submit.name = true : check_submit.name = false;
                if ( ! check_submit.name ) {
                    $("#js_NSmess").empty();
                    $("#js_NSmess").append("<strong>Имя</strong> может содержать только символы&#58;<br><strong>Кириллицы&#44; Латиницы");
                    $("#closeDNS").parent().fadeIn("slow");
                }
                else $("#closeDNS").parent().fadeOut("slow");
                
                if ( !inputsname ) {
                    if ( check_submit.name ) {
                        check_submit.surname = false;
                        $("#js_NSmess").empty();
                        $("#js_NSmess").append("<strong>Фамилия</strong> не может отсутствовать");
                        $("#closeDNS").parent().fadeIn("slow");
                    }
                }
                
                
                
            }
            else {
                if ( inputsname ) {
                    check_submit.name = false;
                    $("#js_NSmess").empty();
                    $("#js_NSmess").append("<strong>Имя</strong> не может отсутствовать");
                    $("#closeDNS").parent().fadeIn("slow");
                }
                else check_submit.surname = true;
            }
            
            submitActivateCheck( check_submit );
            
            
            
            
            
            
        
            
            
            
            
        
            
            //pattern.test( inputname ) ? check_submit.ns = true : check_submit.ns = false;
            //if ( ! check_submit.ns && inputname ) {
                //$("#js_NSmess").empty();
                //$("#js_NSmess").append("<strong>Имя</strong> может содержать только символы&#58;<br><strong>Кириллицы&#44; Латиницы");
                //$("#closeDNS").parent().fadeIn("slow");
            //}
            //else {
                //$("#id_surname").val() ? check_submit.ns = true : check_submit.ns = false;
                //if ( check_submit.ns ) {
                    //$("#js_NSmess").empty();
                    //$("#js_NSmess").append("<strong>Имя</strong> не может отсутствовать");
                    //$("#closeDNS").parent().fadeIn("slow");
                //}
                //else $("#closeDNS").parent().fadeOut("slow");
            //}
            //submitActivateCheck( check_submit );
        }
    );
    $("#id_surname").change(
        function(event) {
            var pattern = /^[a-zA-Z\u0400-\u04ff]+$/;
            var inputname = $( "#id_name" ).val();
            var inputsname = $( this ).val();
            
            if ( inputsname ) {
                
                pattern.test( inputsname ) ? check_submit.surname = true : check_submit.surname = false;
                if ( ! check_submit.surname ) {
                    $("#js_NSmess").empty();
                    $("#js_NSmess").append("<strong>Фамилия</strong> может содержать только символы&#58;<br><strong>Кириллицы&#44; Латиницы");
                    $("#closeDNS").parent().fadeIn("slow");
                }
                else $("#closeDNS").parent().fadeOut("slow");
                
                if ( !inputname ) {
                    if ( check_submit.surname ) {
                        check_submit.name = false;
                        $("#js_NSmess").empty();
                        $("#js_NSmess").append("<strong>Имя</strong> не может отсутствовать");
                        $("#closeDNS").parent().fadeIn("slow");
                    }
                }
                
                
                
            }
            else {
                if ( inputname ) {
                    check_submit.surname = false;
                    $("#js_NSmess").empty();
                    $("#js_NSmess").append("<strong>Фамилия</strong> не может отсутствовать");
                    $("#closeDNS").parent().fadeIn("slow");
                }
                else check_submit.name = true;
            }
            
            submitActivateCheck( check_submit );
            
            //pattern.test( inputsname ) ? check_submit.ns = true : check_submit.ns = false;
            //if ( ! check_submit.ns && inputsname ) {
                
                //$("#js_NSmess").empty();
                //$("#js_NSmess").append("<strong>Фамилия</strong> может содержать только символы&#58;<br><strong>Кириллицы&#44; Латиницы");
                //$("#closeDNS").parent().fadeIn("slow");
            //}
            //else {
                
                //$("#id_name").val() ? check_submit.ns = true : check_submit.ns = false;
                //if ( check_submit.ns ) {
                    //$("#js_NSmess").empty();
                    //$("#js_NSmess").append("<strong>Фамилия</strong> не может отсутствовать");
                    //$("#closeDNS").parent().fadeIn("slow");
                //}
                //else {
                    
                    //$("#closeDNS").parent().fadeOut("slow");
                //}
            //}
            //submitActivateCheck( check_submit );
        }
    );
});
