# -*- coding: utf-8 -*-
# Email-backend, переопределяющий стандартный django.contrib.auth.backends.ModelBackend
# аутентификации по ( User.username(2) | User.email(1) ) & и User.password полям
from django.contrib.auth.models import User
from django.core.validators import validate_email
from django.core.exceptions import ValidationError

class EmailAuthBackend( object ):
	def authenticate(self, username=None, password=None):
			try:
				validate_email( username )
			except ValidationError:
				try:
					user = User.objects.get( username = username )
				except User.DoesNotExist:
					return None
				return user if user.check_password( password ) else None
			try:
				user = User.objects.get( email = username )
			except User.DoesNotExist:
				return None
			return user if user.check_password( password ) else None

	def get_user(self, user_id):
		try:
			return User.objects.get( pk = user_id )
		except User.DoesNotExist:
			return None