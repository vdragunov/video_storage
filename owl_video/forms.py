# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
from django.core.exceptions import ObjectDoesNotExist
from base_site.owl_video.models import Event
import re

#dict_arr_errors =  [ { 'required' : 'Требуется ввести Логин',
        #'min_length':'Логин не может быть короче 2-х символов',
        #'max_length':'Логин не может быть длинее 16-ти символов'
    #},
    #{'max_length':'Имя не может быть длинее 30-ти символов'},
    #{'max_length':'Фамилия не может быть длинее 30-ти символов'},
    #{'max_length':'Email не может быть длинее 255-ти символов',
        #'invalid':'Email не действителен',
        #'required' : 'Требуется ввести Email'
    #},
    #{'max_length':'Пароль не может быть длинее 32-х символов',
        #'required' : 'Требуется ввести Пароль'
    #},
    #{'max_length':'Пароль не может быть длинее 32-х символов',
        #'required' : 'Требуется ввести еще раз Пароль'
    #}
#]
def get_errors_dict(min_val=2, max_val=255, email=False):
    dict_errors = { 'required' : u'Необходимо заполнить' }
    dict_errors['max_length'] = u'Поле не может быть длинее чем {0} символ.'.format( max_val )
    dict_errors['min_length'] = u'Поле не может быть короче чем {0} символ.'.format( min_val )
    if email: dict_errors['invalid'] = u'Email не действителен'
    return dict_errors


class RegisterForm( forms.Form ):
    userlogin = forms.CharField( min_length = 2, max_length = 16, error_messages = get_errors_dict(2,16), widget=forms.TextInput( attrs={ 'class':'form-control','maxlength':'16','placeholder':'Введите псевдоним' } ) )
    name = forms.CharField( required = False, max_length = 30, error_messages = get_errors_dict(max_val=30), widget=forms.TextInput( attrs={ 'class':'form-control','maxlength':'30','placeholder':'Введите Ваше имя' } ) )
    surname = forms.CharField( required = False, max_length = 30, error_messages = get_errors_dict(max_val=30), widget=forms.TextInput( attrs={ 'class':'form-control','maxlength':'30','placeholder':'Введите Вашу фамилию' } ) )
    email = forms.EmailField( max_length = 255, error_messages = get_errors_dict( email=True ), widget=forms.TextInput( attrs={ 'class':'form-control','maxlength':'255','placeholder':'E-mail' } ) )
    #TODO email field, in django 1.6,, has EmailInput widget
    f_password = forms.CharField( max_length = 32, error_messages = get_errors_dict(max_val=32), widget=forms.PasswordInput( attrs={ 'class':'form-control','maxlength':'32','placeholder':'Ваш пароль' } ) )
    s_password = forms.CharField( max_length = 32, error_messages = get_errors_dict(max_val=32), widget=forms.PasswordInput( attrs={ 'class':'form-control','maxlength':'32','placeholder':'Ваш пароль, еще раз' } ) )
    
    def clean_name( self ):
        usname = self.cleaned_data.get( "surname", "" )
        uname = self.cleaned_data.get( "name", "" )
        match = re.compile(ur'^[a-zA-Z\u0400-\u04ff]*$')
        
        if not ( match.search( uname ) ) :
            raise forms.ValidationError(u"Поле может содержать только Кириллицу, Латиницу")
            
            
        #~ elif  ( not uname ) and ( usname != '' ) :
            #~ raise forms.ValidationError(u"Вы забыли ввести Ваше имя")
            
            
        return uname
    
    def clean_surname( self ):
        usname = self.cleaned_data.get( "surname", "" )
        uname = self.cleaned_data.get( "name", "" )
        match = re.compile(ur'^[a-zA-Z\u0400-\u04ff]*$')
        
        if not ( match.search( usname ) ) :
            raise forms.ValidationError(u"Поле может содержать только Кириллицу, Латиницу")
            
        #~ elif ( not usname ) and ( uname != '' ) :
            #~ raise forms.ValidationError(u"Вы забыли ввести Вашу фамилию")
            
            
        return usname
        
    def clean_userlogin( self ):
        ulogin = self.cleaned_data.get( "userlogin", "" )
        match = re.compile(ur'^[a-zA-Z0-9\+\.\-@_]+$')
        if not ( match.search( ulogin ) ) :
            raise forms.ValidationError(u"Разрешено использовать Латиницу, цифры и знаки: @ + - _ .")
            return ulogin
            
        try:
            User.objects.get( username = ulogin )
        except ObjectDoesNotExist:
            return ulogin
            
        raise forms.ValidationError(u"Логин: \'{0}\' уже занят".format( ulogin ) )
        return ulogin
        
    
    def clean_s_password( self ):
        f_pass = self.cleaned_data.get( "f_password", "" )
        s_pass = self.cleaned_data.get( "s_password", "" )
        match = re.compile(r'^\w+$')
        if not ( match.search( s_pass ) ) :
            raise forms.ValidationError(u"Разрешены только ASCII символы")
            return s_pass
        
        if f_pass != s_pass :
            raise forms.ValidationError(u"Пароли не совпадают")
            
        return s_pass
        
    def clean_f_password( self ):
        f_pass = self.cleaned_data.get( "f_password", "" )
        match = re.compile(r'^\w+$')
        if not ( match.search( f_pass ) ) :
            raise forms.ValidationError(u"Разрешены только ASCII символы")
        return f_pass

class LoginForm( forms.Form ):
    login_username = forms.CharField( min_length = 2, max_length = 70, error_messages = get_errors_dict(2,70), widget=forms.TextInput( attrs={ 'class':'form-control','maxlength':'70','placeholder':'Псевдоним' } ) )
    login_pass = forms.CharField( max_length = 32, error_messages = get_errors_dict(max_val=32), widget=forms.PasswordInput( attrs={ 'class':'form-control','maxlength':'32','placeholder':'Пароль' } ) )
    
    def clean_username( self ):
        username = self.cleaned_data.get( "login_username", "" )
        match = re.compile(r'^[a-zA-Z0-9\+\.\-@_]+$')
        if not ( match.search( username ) ) :
            raise forms.ValidationError(u"Разрешено использовать Латиницу, цифры и знаки: @ + - _ .")
        return username
        
    def clean_login_pass( self ):
        password = self.cleaned_data.get( "login_pass", "" )
        match = re.compile(r'^\w+$')
        if not ( match.search( password ) ) :
            raise forms.ValidationError(u"Разрешены только ASCII символы")
        return password

class PassRemindForm( forms.Form ):
    username = forms.CharField( min_length = 2, max_length = 16, error_messages = get_errors_dict(2,16), widget=forms.TextInput( attrs={ 'class':'form-control','maxlength':'16','placeholder':'Введите псевдоним' } ) )     
    email = forms.EmailField( max_length = 255, error_messages = get_errors_dict( email=True ), widget=forms.TextInput( attrs={ 'class':'form-control','maxlength':'255','placeholder':'E-mail' } ) )
        
    def clean_username( self ):
        uname = self.cleaned_data.get( "username", "" )
        match = re.compile(ur'^[a-zA-Z0-9\+\.\-@_]+$')
        if not ( match.search( uname ) ):
            raise forms.ValidationError(u"Разрешено использовать Латиницу, цифры и знаки: @ + - _ .")
        return uname

class SearchForm( forms.Form ):
    q = forms.CharField( required = False, max_length = 255, error_messages = get_errors_dict(), widget=forms.TextInput( attrs={ 'class':'form-control','maxlength':'255','placeholder':'Поиск по названию' } ) )
    search = forms.CharField( required = False, max_length = 255, error_messages = get_errors_dict(), widget=forms.HiddenInput( attrs={ 'value' : 'title' } ) )
    
    def clean_q( self ):
        query = self.cleaned_data.get( "q", "" )
        match = re.compile(ur'^[a-zA-Z0-9\u0400-\u04ff\+\.\-@_]*$')
        if not ( match.search( query ) ):
            raise forms.ValidationError(u"Для поиска разрешено использовать Латиницу, Кириллицу, цифры и знаки: @ + - _ .")
        return query

class CreateEventForm( forms.Form ):
    event_name = forms.CharField( min_length = 2, max_length = 255, error_messages = get_errors_dict(), widget=forms.TextInput( attrs={ 'class':'form-control','maxlength':'255','placeholder':'Введите имя события' } ) )
    event_desc = forms.CharField( required = False, max_length = 3000, error_messages = get_errors_dict( max_val = 3000 ), widget=forms.HiddenInput() )
    
    def clean_event_name( self ):
        event = self.cleaned_data.get( "event_name", "" )
        match = re.compile(ur'^[a-zA-Z0-9\u0400-\u04ff\+\.\-_]{2,255}$')
        if not ( match.search( event ) ):
            raise forms.ValidationError(u"<strong>Имя события</strong> может содержать только цифры и/или символы&#58; Кириллицы&#44; Латиницы&#44; + - _ .")
        try:
            Event.objects.get( name = event )
        except ObjectDoesNotExist:
            return event
        raise forms.ValidationError(u"Событие&#58; <strong>{event_name}</strong> уже существует".format( event_name = event ) )
        return event

class EditVideoDescForm( forms.Form ):
    video_desc = forms.CharField( required = False, max_length = 3000, error_messages = get_errors_dict( max_val = 3000 ), widget=forms.HiddenInput() )

class ChangeAccountDataForm( forms.Form ):
    pass

class ChangeAccountPassForm( forms.Form ):
    current_user_id = forms.CharField( widget=forms.HiddenInput() )
    current_pass = forms.CharField( max_length = 32, error_messages = get_errors_dict(max_val=32), widget=forms.PasswordInput( attrs={ 'class':'form-control','maxlength':'32','placeholder':'Введите текущий пароль' } ) )
    f_new_password = forms.CharField( max_length = 32, error_messages = get_errors_dict(max_val=32), widget=forms.PasswordInput( attrs={ 'class':'form-control','maxlength':'32','placeholder':'Введите новый пароль' } ) )
    s_new_password = forms.CharField( max_length = 32, error_messages = get_errors_dict(max_val=32), widget=forms.PasswordInput( attrs={ 'class':'form-control','maxlength':'32','placeholder':'Введите новый пароль, еще раз' } ) )
    
    def clean_s_new_password( self ):
        f_pass = self.cleaned_data.get( "f_new_password", "" )
        s_pass = self.cleaned_data.get( "s_new_password", "" )
        match = re.compile(r'^\w+$')
        if not ( match.search( s_pass ) ) :
            raise forms.ValidationError(u"Разрешены только ASCII символы")
            return s_pass
        
        if f_pass != s_pass :
            raise forms.ValidationError(u"Пароли не совпадают")
            
        return s_pass
        
    def clean_f_new_password( self ):
        f_pass = self.cleaned_data.get( "f_new_password", "" )
        match = re.compile(r'^\w+$')
        if not ( match.search( f_pass ) ) :
            raise forms.ValidationError(u"Разрешены только ASCII символы")
        return f_pass
    
    def clean_current_pass( self ):
        c_pass = self.cleaned_data.get( "current_pass", "" )
        match = re.compile(r'^\w+$')
        if not ( match.search( c_pass ) ) :
            raise forms.ValidationError(u"Разрешены только ASCII символы")
            
        user = User.objects.get( pk = self.cleaned_data.get( "current_user_id", "" ) )
        if not ( check_password( c_pass, user.password ) ) :
            raise forms.ValidationError(u"Неправильный пароль!")
        
        return c_pass
    
