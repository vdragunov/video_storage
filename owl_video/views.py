# -*- coding: utf-8 -*-
import os
import httplib2
import string
import json, pickle
from hashlib import sha1
from random import shuffle, choice
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse, HttpResponseRedirect, Http404, HttpResponseNotFound
from django.contrib.auth.models import User
from django.contrib.auth.hashers import is_password_usable
from django.contrib import auth
from django.core.mail import EmailMessage
from django.core.serializers.json import DjangoJSONEncoder
from oauth2client.django_orm import Storage
from apiclient.discovery import build #when creating an instance of service, may SSL error occurs 185090050, sudo chmod +x /usr/local/lib/python2.7/dist-packages/httplib2-0.8-py2.7.egg/httplib2/cacerts.txt
from oauth2client.client import flow_from_clientsecrets
from base_site.owl_video.forms import LoginForm, RegisterForm, SearchForm, CreateEventForm, EditVideoDescForm
from base_site.owl_video.models import Entry, Project, StorageInstance, Video, Event, Oauth2Credential
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from guardian.shortcuts import get_objects_for_group, get_objects_for_user, remove_perm, assign_perm, get_perms, get_perms_for_model

CLIENT_SECRETS = os.path.join( os.path.dirname(__file__), '..', 'etc', 'client_secrets.json' ) #'../etc/client_secrets.json'
LOCAL_APP_TOKEN = 'YRXIV6D9KUGON97IS6LB3ZQD8WH6CXUW'

GOOGLE_FLOW = flow_from_clientsecrets( CLIENT_SECRETS, scope=['https://www.googleapis.com/auth/userinfo.email',
'https://www.googleapis.com/auth/userinfo.profile',
'https://www.googleapis.com/auth/youtube'],
redirect_uri='http://sova.auditory.co/account/google_callback')

PERMS = { 
    'Project' : get_perms_for_model( Project ).values_list( 'codename', flat = True ),
    'StorageInstance' : get_perms_for_model( StorageInstance ).values_list( 'codename', flat = True ),
    'Entry' : get_perms_for_model( Entry ).values_list( 'codename', flat = True ),
    'Event' : get_perms_for_model( Event ).values_list( 'codename', flat = True )
}

class NumPagePaginator():
    def __init__(self, arr_of_pages, quantity):
        self.pages = arr_of_pages
        self.amt = quantity
        self.page_portions = []
        self.length = len( arr_of_pages ) 
        if ( self.length <= self.amt ):
            self.page_portions.append( self.pages )
        else:
            self.page_portions.append( [] )
            j, i_amt = 0, 0
            for i in xrange( self.length ):
                i_amt += 1
                if ( i_amt < self.amt ):
                    self.page_portions[ j ].append( self.pages[ i ] )
                else:
                    i_amt = 1
                    self.page_portions[ j ].append( self.pages[ i ] )
                    j += 1
                    self.page_portions.append( [] )
                    self.page_portions[ j ].append( self.pages[ i ] )
                    
        self.real_amt = len( self.page_portions )
        
    def getPagesPortion( self, portion = 1 ):
        if ( portion <= 0 ): return self.page_portions[ 0 ]
        else: return self.page_portions[ portion - 1 ]
        
    def getAmtOfPortions( self ):
        return self.real_amt
        
    def getAllPortions( self ):
        return self.page_portions
        

def ajaxUserNameResolver( request ) :
    new_login = request.POST.get( "new_login", "" )
    try:
        User.objects.get( username = new_login )
    except ObjectDoesNotExist:
        return HttpResponse( "ok" )
    
    return HttpResponse( "error_existed" )
    
def ajaxEventNameResolver( request ) :
    new_event = request.GET.get( "new_event", "" )
    try:
        Event.objects.get( name = new_event )
    except ObjectDoesNotExist:
        return HttpResponse( "ok" )
    
    return HttpResponse( "error_existed" )
    
#~ def ajaxUserLoginCheck( request ):
    #~ if request.user.is_authenticated(): return HttpResponse( "ok" )
    #~ else: return HttpResponse( "not_logged_in" )

def showIndex( request ):
    
    user_join = False
    
    events = Event.objects.all()[:3]
    
    if hasattr( request, "session" ):
        user_join = request.session.get('user_join', False)
        if user_join: del request.session['user_join']
    
    return render_to_response( "base_index.html", { "page_title" : "Видеоархив", "user_join" : user_join, "index_events" : events  }, context_instance = RequestContext( request ) ) 

def userRegister( request ):
    if request.user.is_authenticated():
        return HttpResponseRedirect( "/" ) #TODO next=<previous_user_path_link>
    form = RegisterForm()
    if request.method == 'POST':
        form = RegisterForm( request.POST )
        if form.is_valid():
            user = User.objects.create_user( username = form.cleaned_data['userlogin'], email = form.cleaned_data['email'], password = form.cleaned_data['f_password'] )
            user.first_name = form.cleaned_data.get('name','')
            user.last_name = form.cleaned_data.get('surname','')
            user.save()
            if hasattr( request, "session" ):
                request.session['user_join'] = True
                
            email_temp = get_template('email.html')
            email_cntxt = Context( { "username" : user.username, "user_pass" : form.cleaned_data['f_password'], "videostorage_url" : 'http://' + request.get_host() } )
            email_body = email_temp.render( email_cntxt )
            email_obj = EmailMessage('Регистрация', email_body, to=[user.email])
            email_obj.content_subtype = "html"
            email_obj.send()
                
            return HttpResponseRedirect( reverse( 'base_site.owl_video.views.userLogin' ) )
            
    return render_to_response( "base_register.html", { "page_title" : "Регистрация", "form" : form }, context_instance = RequestContext( request ) ) 
    
#~ def userRegisterCongratulate( request ):
    #~ if request.session:
        #~ user_id = request.session.get('new_user_id', None)
        #~ if user_id is not None:
            #~ render_to_response( "base_index.html", { "page_title" : "Поздравляем!" }, context_instance = RequestContext( request ) ) 
    
def userLogin( request ):
    if request.user.is_authenticated():
        return HttpResponseRedirect( "/" )

    user_join = False
    auth_error = False
    form = LoginForm()
    
    if hasattr( request, "session" ):
        user_join = request.session.get('user_join', False)
        if user_join: del request.session['user_join']
            
    if request.method == 'POST':
        form = LoginForm( request.POST )
        
        if form.is_valid():
            user = auth.authenticate( username = form.cleaned_data['login_username'], password = form.cleaned_data['login_pass'] )
            
            if user is not None and user.is_active:
                auth.login( request, user )
                return HttpResponseRedirect( "/" )
            else:
                auth_error = True
                
    return render_to_response( "base_login.html", { "page_title" : "Вход", "form" : form, "authenticate_error" : auth_error, "user_join" : user_join }, context_instance = RequestContext( request ) ) 
    
def userLogout( request ):
    if request.user.is_authenticated():
        auth.logout( request )
    return HttpResponseRedirect( "/" )

    
def userGrantAccessGoogle( request ):
    owl_video_csrf_token = ''.join( choice( string.ascii_uppercase + string.digits ) for x in xrange( 30 ) ) #our NOT CSRF token, token is for prevent manual link to callBackHandlerFromGoogle
    request.session['ow_state'] = owl_video_csrf_token
    
    if request.user.is_authenticated():
        storage = Storage(Oauth2Credential, 'id', request.user, 'credential')
        credential = storage.get()
        if credential is None or credential.invalid == True:
            #GOOGLE_FLOW.params['display'] = 'popup'
            GOOGLE_FLOW.params['access_type'] = 'offline'
            GOOGLE_FLOW.params['state'] = owl_video_csrf_token
            authorize_url = GOOGLE_FLOW.step1_get_authorize_url()
            return HttpResponseRedirect( authorize_url )
        else:
            HttpResponseRedirect( "/" )
    else:
        #GOOGLE_FLOW.params['display'] = 'popup'
        GOOGLE_FLOW.params['access_type'] = 'offline'
        #GOOGLE_FLOW.params['approval_prompt'] = 'force'
        GOOGLE_FLOW.params['state'] = owl_video_csrf_token
        authorize_url = GOOGLE_FLOW.step1_get_authorize_url()
        return HttpResponseRedirect( authorize_url )
    
def callBackHandlerFromGoogle( request ):
    #create new user with credentials and login or check user by email and login with existed credentials
    if ( request.REQUEST.get('state','') == request.session.get('ow_state','') ) and request.session.get('ow_state',''):
        
        credential = GOOGLE_FLOW.step2_exchange( request.REQUEST )
        
        if request.user.is_authenticated():
            
            storage = Storage(Oauth2Credential, 'id', request.user, 'credential') #стандартная лабуда
            
            current_rtoken = request.user.oauth2credential_set.get().rtoken #временно сохраняем refresh_token или пустую строку (костыль для django framework`а от гугла)
            
            storage.put( credential ) #стандартная лабуда
            
            oauth2credential = request.user.oauth2credential_set.get()
            if credential.refresh_token:
                oauth2credential.rtoken = credential.refresh_token #сохраняем новый refresh_token отдельно в БД
                oauth2credential.save()
            elif current_rtoken:
                oauth2credential.rtoken = current_rtoken #проверяем и возвращаем refresh_token на место в БД
                oauth2credential.save()
            
        else:
            
            http = httplib2.Http()
            http = credential.authorize( http )
            service = build("oauth2", "v2", http = http)
            response = service.userinfo().get().execute() #get one element collection
            current_rtoken = None
            
            if not User.objects.filter( email = response['email'] ).exists():
                
                new_username_id = User.objects.order_by( '-id' )[0].id + 1
                while User.objects.filter( username = 'user_' + str( new_username_id ) ).exists():
                    new_username_id += 1

                generated_username = 'user_' + str( new_username_id )
                pl = [ l for l in sha1( generated_username ).hexdigest()[:7] ]
                shuffle( pl )
                generated_password = ''.join( pl )
                user = User.objects.create_user( username = generated_username, password = generated_password )  #make new 'user_<id>'
                user.email = response['email']
                user.first_name = response['given_name']
                user.last_name = response['family_name']
                user.save()
                
                storage = Storage(Oauth2Credential, 'id', user, 'credential')
                
                current_rtoken = user.oauth2credential_set.get().rtoken
                
                storage.put( credential )
                
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                auth.login( request, user )
                if hasattr( request, "session" ):
                    request.session['user_join'] = True
                    
                email_temp = get_template('email.html')
                email_cntxt = Context( { "username" : user.username, "user_pass" : generated_password, "videostorage_url" : 'http://' + request.get_host() } )
                email_body = email_temp.render( email_cntxt )
                email_obj = EmailMessage('Регистрация', email_body, to=[user.email])
                email_obj.content_subtype = "html"
                email_obj.send()
                    
            else:
                user = User.objects.get( email = response['email'] )
                storage = Storage(Oauth2Credential, 'id', user, 'credential')
                
                current_rtoken = user.oauth2credential_set.get().rtoken
                #existed_credential = storage.get()
                storage.put( credential )
                #if existed_credential is None or existed_credential.invalid == True: storage.put( credential )
                
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                auth.login( request, user )
            
            oauth2credential = user.oauth2credential_set.get()
            if credential.refresh_token:
                oauth2credential.rtoken = credential.refresh_token #сохраняем новый refresh_token отдельно в БД
                oauth2credential.save()
            elif current_rtoken:
                oauth2credential.rtoken = current_rtoken #проверяем и возвращаем refresh_token на место в БД
                oauth2credential.save()
                
        del request.session['ow_state']
    return HttpResponseRedirect( "/" )


def viewAccount(request, acc):
    acc_id = int( acc )
    
    #~ if User.objects.filter( pk = acc_id ).exists():
        #~ user = User.objects.get(pk = acc_id)
        #~ return HttpResponse( "user : " + user.username + "<br>email : " + user.email )
    #~ raise Http404
    try:
        shown_user = User.objects.get(pk = acc_id)
    except ObjectDoesNotExist:
        raise Http404
    
    
    
    if ( request.user.is_authenticated() and request.user.id == shown_user.id ):
        template = "base_account.html"
        create_event_form = CreateEventForm()
        if request.method == 'POST':
            create_event_form = CreateEventForm( request.POST )
            if create_event_form.is_valid():
                event = Event.objects.create( name = create_event_form.cleaned_data['event_name'], description = create_event_form.cleaned_data['event_desc'], creator = request.user )
                return HttpResponseRedirect( reverse( 'base_site.owl_video.views.viewAccount', args=[request.user.id] ) )
    else:
        create_event_form = None
        template = "base_anonym_account.html"
    
    return render_to_response( template, { "page_title" : u"Пользователь: " + shown_user.username, "shown_user" : shown_user, "create_event_form" : create_event_form }, context_instance = RequestContext( request ) )

def viewEventList( request ):
    
    events = Event.objects.all()
    
    return render_to_response( "base_eventlist.html", { "page_title" : "Просмотр событий", "events" : events }, context_instance = RequestContext( request ) )

def viewVideoList( request ):
    #~ check search query
    
    search_form = SearchForm()
    search_errors = []
    
    try:
        query = request.GET['q']
        search_type = request.GET.get( 'search', 'title' )
        search_form = SearchForm( {'q' : query, 'search' : search_type } )
        if search_form.is_valid():
            
            search_type = search_form.cleaned_data['search']
            search_query = search_form.cleaned_data['q']
            
            video_list = dict()
            
            if ( search_type == 'user' ):
                try:
                    user = User.objects.get( username = search_query )
                    
                    video_list = user.entry_set.all()
                    if not( video_list.exists() ):
                        search_errors.append( u'Пользователь <strong>&#34;' + search_query + u'&#34;</strong> пока не добавил ни одной видеозаписи' ) #TODO insert link to profile TODO add utf-8 to User.username
                        video_list = Entry.objects.all()
                    
                except ObjectDoesNotExist:
                    search_errors.append( u'Пользователя <strong>&#34;' + search_query + u'&#34;</strong> не существует' )
                    video_list = Entry.objects.all()
                    
            elif ( search_type == 'event' ):
                try:
                    event = Event.objects.get( name = search_query )
                    
                    video_list = event.entrys.all()
                    if not( video_list.exists() ):
                        search_errors.append( u'Ничего не найдено для события <strong>&#34;' + search_query + u'&#34;</strong>' )
                        video_list = Entry.objects.all()
                    
                except ObjectDoesNotExist:
                    search_errors.append( u'События <strong>&#34;' + search_query + u'&#34;</strong> не существует' )
                    video_list = Entry.objects.all()
                    
            elif ( search_type == 'title' ):
                video_list = Entry.objects.filter( title__icontains = search_query )
                if not( video_list.exists() ):
                    search_errors.append( u'Ничего не найдено по запросу <strong>&#34;' + search_query + u'&#34;</strong>' )
                    video_list = Entry.objects.all()
                
            else: 
                video_list = Entry.objects.all()
                search_errors.append( u'Задан неправильный параметр запроса <strong>&#34;' + search_type + u'&#34;</strong>' )
        else:
            video_list = Entry.objects.all()
                
    except KeyError:
        video_list = Entry.objects.all()
        
    #~ set view entry params
    video_list_amt = len( video_list )
    video_list_all = False
    
    try:
        per_page = request.GET['amt']
        if hasattr( request, "session" ):
            request.session['view_amt'] = per_page
    except KeyError:
        per_page = 2
        if hasattr( request, "session" ):
            per_page = request.session.get( 'view_amt', per_page )
    
    try:
        per_page = abs( int( per_page ) )
    except ValueError:
        per_page = 'all'
    
    #~ paginator = Paginator( video_list, video_list_amt ) if ( per_page == 'all' ) else Paginator( video_list, per_page )
    
    if ( per_page == 'all' ):
        videos = video_list
        video_list_all = True
        page_of_pages = False
    else:
        paginator = Paginator( video_list, per_page )
        try:
            page = abs ( int( request.GET.get( 'page', 1 ) ) )
        except ValueError:
            page = 1
        
        try:
            videos = paginator.page( page )
        except EmptyPage:
            videos = paginator.page( paginator.num_pages )
            
        view_paginator = NumPagePaginator( paginator.page_range, 3 )
        
        page_of_pages = view_paginator.getPagesPortion( 1 )
        
        for portion in view_paginator.getAllPortions():
            
            if ( ( page in portion ) and ( len( portion ) > 1 ) ): page_of_pages = portion
                
        #~ page_of_pages = view_paginator.page( page / 3 + 1 )
        
        #~ if not( page % 2 ):
            #~ page_of_pages = view_paginator.page( page / 2 + 1 )
        #~ else:
            #~ page_of_pages
        
    

    return render_to_response( "base_videolist.html", { "page_title" : "Список видеозаписей", "query_errors" : search_errors, "search_form" : search_form, "videolist" : videos, "list_all" : video_list_all, "per_page" : per_page, "page_of_pages" : page_of_pages }, context_instance = RequestContext( request ) ) 

def viewVideoEntry( request, entry_id ):
    video_id = int( entry_id )
    try:
        entry_video = Entry.objects.get( pk = video_id )
    except ObjectDoesNotExist:
        raise Http404
    
    
    create_event_form = CreateEventForm()
    edit_desc_form = EditVideoDescForm()
    select_event_errors = []
    exclude_event_list = []
    
    if ( request.user.is_authenticated() and request.user.id == entry_video.owner.id ):
        exclude_event_list = Event.objects.exclude( videos__id__exact = video_id )
        if ( request.method == 'POST'):
            if ( 'edit_event_submit' in request.POST ):
                
                edit_events_id = request.POST.getlist('edit_events', '')
                if not edit_events_id:
                    entry_video.event_set.clear()
                else:
                    submit_events = []
                    connected_entry_video_events = entry_video.event_set.all()
                    
                    for event_str_id in edit_events_id:
                        
                        try:
                            event_id = int( event_str_id )
                            event = Event.objects.get( pk = event_id )
                            submit_events.append( event )
                        except ValueError:
                            continue
                        except ObjectDoesNotExist:
                            raise Http404
                        if ( event not in connected_entry_video_events ) : entry_video.event_set.add( event )
                        
                    for connected_event in connected_entry_video_events:
                        if ( connected_event not in submit_events ) : entry_video.event_set.remove( connected_event )
                        
                
            elif ( 'create_event_submit' in request.POST ):
                create_event_form = CreateEventForm( request.POST )
                
                if create_event_form.is_valid():
                    
                    event = Event.objects.create( name = create_event_form.cleaned_data['event_name'], description = create_event_form.cleaned_data['event_desc'], creator = request.user )
                    
                    return HttpResponseRedirect( reverse( 'base_site.owl_video.views.viewVideoEntry', args=[video_id] ) )
            elif ( 'edit_desc_submit' in request.POST ):
                edit_desc_form = EditVideoDescForm( request.POST )
                
                if edit_desc_form.is_valid():
                    entry_video.description = edit_desc_form.cleaned_data['video_desc']
                    entry_video.save()
    
    return render_to_response( "base_video.html", { "page_title" : "Видеозапись", "entry_video" : entry_video, "event_list" : exclude_event_list, "create_event_form" : create_event_form, "edit_desc_form" : edit_desc_form, "select_event_errors" : select_event_errors  }, context_instance = RequestContext( request ) ) 

def viewEventEntry( request, entry_id ):
    event_id = int( entry_id )
    
    try:
        entry_event = Event.objects.get( pk = event_id )
    except ObjectDoesNotExist:
        raise Http404
    return render_to_response( "base_event.html", { "page_title" : "Событие", "entry_event" : entry_event }, context_instance = RequestContext( request ) ) 

@csrf_exempt    
def sendLocalAppCreds( request ):
    if request.method == 'POST':
        try:
            local_app_data = json.loads( request.body )
        except ValueError:
            raise Http404
        if local_app_data.get( 'app_token', '' ) == LOCAL_APP_TOKEN:

            remember_me_auth = { 'hashed' : local_app_data.get('hashed', False), 'remember_me' : local_app_data.get('remember_me', False) }

            data = dict()

            if remember_me_auth['hashed']:
                if is_password_usable( local_app_data.get( 'pass', '' ) ):
                    try:
                        user = User.objects.get( pk = local_app_data['user_id'] )
                    except ObjectDoesNotExist:
                        data['status'] = 'auth_error'
                        return HttpResponse( json.dumps( data ), content_type="application/json" )
            else:
                user = auth.authenticate( username = local_app_data.get( 'uname', '' ), password = local_app_data.get( 'pass', '' ) )

            #storage = Storage(Oauth2Credential, 'id', user, 'credential') # Заглушка для локального тестирования local_app
            #credential = storage.get()
            #credential.refresh_token = user.oauth2credential_set.get().rtoken
            
            
            
            # if ( ( user is not None ) and user.is_active and ( credential is not None ) ) :
            if ( ( user is not None ) and user.is_active ) : #debug^
                if remember_me_auth['remember_me']: data['hash'] = user.password
                data['status'] = 'ok'
                data['user_id'] = user.id
                data['user_login'] = user.username
                data['user_link'] = 'http://' + request.get_host() + reverse( 'base_site.owl_video.views.viewAccount', args=[user.id] )
                #ata['pickled_credential'] = pickle.dumps( credential ) # debug
                return HttpResponse( json.dumps( data ), content_type="application/json" )
            else:
                data['status'] = 'auth_error'
                return HttpResponse( json.dumps( data ), content_type="application/json" )
    raise Http404

@csrf_exempt    
def reserveVideoId( request ):
    if request.method == 'POST':
        try:
            local_app_data = json.loads( request.body )
        except ValueError:
            raise Http404
        if local_app_data.get( 'app_token', '' ) == LOCAL_APP_TOKEN:
            reserved_ids = []
            user = User.objects.get( pk = local_app_data['user_id'] )
            amt = int( local_app_data['video_amt'] )
            for video_i in xrange( amt ):
                v = Video.objects.create( title = local_app_data['video_titles'][video_i], owner = user, status = 'reserved' )
                reserved_ids.append(v.id)
            data = {
                'status' : 'reserved',
                'video_ids' : reserved_ids
            }
            return HttpResponse( json.dumps( data ), content_type="application/json" )
    raise Http404
    
@csrf_exempt
def additionalVideoData( request ):
    if request.method == 'POST':
        try:
            local_app_data = json.loads( request.body )
        except ValueError:
            raise Http404
        if local_app_data.get( 'app_token', '' ) == LOCAL_APP_TOKEN:
            amt = int(local_app_data['video_amt'] )
            for video_i in xrange( amt ):
                key_id = long( local_app_data['video_ids'][video_i] )
                
                Video.objects.filter( pk = key_id ).update( raw_title = local_app_data['video_raw_titles'][video_i] )
                Video.objects.filter( pk = key_id ).update( youtube_ui = local_app_data['youtube_uis'][video_i] )
                Video.objects.filter( pk = key_id ).update( status = 'uploaded' )
                
                #v = Video.objects.get( pk = long( local_app_data['video_ids'][video_i] ) )
                
                #~ v.youtube_ui = local_app_data['youtube_uis'][video_i]
                #~ v.raw_title = local_app_data['video_raw_titles'][video_i]
                #~ v.status = 'uploaded'
                #~ v.save() not efficiently db query!
            
            return HttpResponse( json.dumps( { 'status' : 'uploaded' } ), content_type="application/json" )
    raise Http404

@csrf_exempt
def getUserAccessArchive( request ):
    if request.method == 'POST':
        try:
            local_app_data = json.loads( request.body )
        except ValueError:
            raise Http404
        if ( local_app_data.get( 'app_token', '' ) == LOCAL_APP_TOKEN ) and bool( local_app_data.get( 'user_id', '' ) ):

            try: user = User.objects.get( pk = local_app_data['user_id'] )
            except ObjectDoesNotExist: raise Http404

            storage_types = [ 'DD', 'CD', 'SD' ]

            archive = dict()
            archive[ 'user_id' ] = user.id
            archive[ 'user_email' ] = user.email
            archive[ 'type' ] = 'data'
            archive[ 'status' ] = ''
            # all_perms_for_storage = get_perms_for_model( StorageInstance ).values_list( 'codename', flat = True )
            all_viewable_storages = get_objects_for_user( user, 'view_storageinstance', klass = StorageInstance ) # кэш всех StorageInstance где у user есть право "view_storageinstance"
            all_viewable_entrys = get_objects_for_user( user, 'view_entry', klass = Entry )
            all_viewable_projects = get_objects_for_user( user, 'view_project', klass = Project )

            if not all_viewable_storages:
                archive['status'] += 'info: <user id={0} | email={1}> has no permitted storages to view\n'.format( user.id, user.email )
                archive['status'] += 'warn: no objects received for <user id={0} | email={1}> \n'.format( user.id, user.email )
                return HttpResponse( json.dumps( archive ), content_type="application/json" )

            for s_type in xrange( 3 ):

                storages = all_viewable_storages.filter( storage_type = storage_types[ s_type ] ) # кэш всех StorageInstance типа s_type
                
                archive[ storage_types[ s_type ] ] = list( storages.values( 'id', 'name', 'storage_identifier', 'owner__id', 'owner__email', 'add_date',
                 'description', 'all_space', 'free_space', 'state' ) )
                for storage in archive[ storage_types[ s_type ] ]:

                    if not all_viewable_entrys: archive['status'] += 'info: <user id={0} | email={1}> has no permitted entries to view\n'
                    else:
                        #storage[ 'entries' ] = storages.get( pk = storage[ 'id' ] ).entrys.filter( owner = user, project__entrys = None ).values
                        storage[ 'entries' ] = list( all_viewable_entrys.filter( storageinstance__pk = storage[ 'id' ], project__entrys = None ).values( # выбираем все Entry без Project
                            'id', 'title', 'raw_title', 'owner__id', 'owner__email', 'restore_path', 'youtube_ui', 'net_link', 'description',
                            'meta_data', 'add_date', 'modify_date', 'hash_sha1', 'status', 'entry_type', 'size', 'filemark' ) )

                    if not all_viewable_projects: archive['status'] += 'info: <user id={0} | email={1}> has no permitted projects to view\n'
                    else:
                        #fetched_projects = storage.projects.filter( owner = user )
                        fetched_projects = all_viewable_projects.filter( storageinstance__pk = storage[ 'id' ] ) # кэшируем QuerySet, чтобы два раза не обращаться к БД
                        storage[ 'projects' ] = list( fetched_projects.values( 'id', 'title', 'owner__id', 'owner__email', 'restore_path', 'youtube_ui', # выбираем все Project для просмотра юзером
                            'net_link', 'modify_date', 'add_date', 'description', 'status' ) )

                        for project in storage[ 'projects' ]:
                            project[ 'entries' ] = list( fetched_projects.get( pk = project[ 'id' ] ).entrys.all().values( # выбираем все Entry Project
                                'id', 'title', 'raw_title', 'owner__id', 'owner__email', 'restore_path', 'youtube_ui', 'net_link', 'description',
                                'meta_data', 'add_date', 'modify_date', 'hash_sha1', 'status', 'entry_type', 'size', 'filemark' ) )

            archive['status'] += 'info: ok, archive objects received for <user id={0} | email={1}>\n'.format( user.id, user.email )
            return HttpResponse( json.dumps( archive, cls=DjangoJSONEncoder, ensure_ascii = False ), content_type="application/json" )

    raise Http404

@csrf_exempt
def getStorageProjectUserPerm( request ): # получение JSON массива доступных носителей/проектов для бэкапа файлов юзером
    if request.method == 'POST':
        try:
            local_app_data = json.loads( request.body )
        except ValueError:
            raise Http404
        if ( local_app_data.get( 'app_token', '' ) == LOCAL_APP_TOKEN ) and bool( local_app_data.get( 'user_id', '' ) ):
            
            try: user = User.objects.get( pk = local_app_data['user_id'] )

            except ObjectDoesNotExist:
                raise Http404

            storage_types = [ 'DD', 'CD', 'SD' ]
            data = dict()
            data['type'] = 'meta'
            data['perm_codes'] = [ 'add_storageinstance', 'add_project' ]
            data['user_id'] = user.id
            data['status'] = ''

            all_addable_storages = get_objects_for_user( user, 'add_storageinstance', klass = StorageInstance )
            all_addable_projects = get_objects_for_user( user, 'add_project', klass = Project )

            if ( not all_addable_projects ) and ( not all_addable_storages ):
                data['status'] += 'warn: <user id={0} | email={1}> has no allowed places to add objects\n'.format( user.id, user.email )
                return HttpResponse( json.dumps( data ), content_type="application/json" )

            for s_type in xrange( 3 ):
                storages = all_addable_storages.filter( storage_type = storage_types[ s_type ] )
                data[ storage_types[ s_type ] ] = list( storages.values( 'id', 'name', 'storage_identifier', 'owner__email', 'all_space', 'free_space', 'state' ) )

            data['projects'] = list ( all_addable_projects.values( 'id', 'title', 'storageinstance__id', 'owner__email', 'status' ) )
            data['status'] += 'info: ok, archive allowed places received for <user id={0} | email={1}>'.format( user.id, user.email )
            return HttpResponse( json.dumps( data, cls=DjangoJSONEncoder, ensure_ascii = False ), content_type="application/json" )

    raise Http404

@csrf_exempt
def addUserArchiveObject( request ):
    if request.method == 'POST':
        try:
            local_app_data = json.loads( request.body )
        except ValueError:
            raise Http404
        if ( local_app_data.get( 'app_token', '' ) == LOCAL_APP_TOKEN ) and bool( local_app_data.get( 'user_id', '' ) ):

            try: user = User.objects.get( pk = local_app_data['user_id'] )
            except ObjectDoesNotExist: raise Http404

            try: add_type = local_app_data['type']
            except KeyError: raise Http404

            resp = dict()
            resp['status'] = ''

            if add_type == 'entry':
                try:
                    storage = StorageInstance.objects.get( pk = local_app_data['connected_storage'] )
                except (ObjectDoesNotExist, KeyError):
                    resp['status'] += 'error: <storage id=?> not presented\n'
                    return HttpResponse( json.dumps( resp ), content_type="application/json" )

                if local_app_data.get( 'add_type', '' ) == 'project':

                    new_entry = Project( title = local_app_data['title'], owner = user,
                        restore_path = local_app_data['restore_path'],
                        description = local_app_data['description'], status = 'created' )
                    new_entry.save()

                    storage.projects.add( new_project )
                    for perm in PERMS['Project']: assign_perm( perm, user, new_entry )
                else:
                    new_entry = Entry( title = local_app_data['title'], raw_title = local_app_data['raw_title'], owner = user,
                        restore_path = local_app_data['restore_path'], description = local_app_data['description'],
                        meta_data = local_app_data['meta_data'], hash_sha1 = local_app_data['hash_sha1'], status = 'backedup',
                        entry_type = local_app_data['entry_type'], size = local_app_data['size'], filemark = local_app_data['filemark'] )

                    new_entry.save()

                    if local_app_data.get( 'connected_project_id', '' ):
                        try:
                            connected_project = Project.objects.get( pk = local_app_data['connected_project_id'] )
                            connected_project.entrys.add( new_entry )
                        except ObjectDoesNotExist:
                            resp['status'] += 'warn: <project id={0}> doesnt exists, <entry id={1} | title={2}> will only connected to <storage id={3}>\n'.format( local_app_data['connected_project_id'], new_entry.id, new_entry.title, storage.id, )

                    storage.entrys.add( new_entry )
                    for perm in PERMS['Entry']: assign_perm( perm, user, new_entry )

                resp['status'] += 'info: ok, <' + local_app_data['add_type'] + ' id={0} | title={1}> succesfully added\n'.format( new_entry.id, new_entry.title )
                return HttpResponse( json.dumps( resp ), content_type="application/json" )

            elif add_type == 'storage':
                if ( local_app_data.get( 'storage_type', '' ) in [ 'DD', 'CD', 'SD' ] ) and ( local_app_data.get('state', '') in ['CF','SF','UN'] ):

                    new_storage = StorageInstance( name = local_app_data['name'], storage_identifier = local_app_data['storage_identifier'],
                        owner = user, description = local_app_data['description'], all_space = local_app_data['all_space'],
                        free_space = local_app_data['free_space'], state = local_app_data['state'], storage_type = local_app_data['storage_type'] )

                    new_storage.save()
                    for perm in PERMS['StorageInstance']: assign_perm( perm, user, new_storage )

                    resp['status'] += 'info: ok, <storage id={0} | storage_type={1}> succesfully added'.format( new_storage.id, new_storage.storage_type )
                else:
                    resp['status'] += 'error: wrong storage type or state for new <storage name={0}>\n'.format( local_app_data['name'] )

                return HttpResponse( json.dumps( resp ), content_type="application/json" )                    

    raise Http404
    
#~ def passRemind( request):
    #~ search_user_error = False
    #~ form = PassRemindForm()
    #~ if not User.objects.filter( username = response['username'] ).exists():
        #~ search_user_error = True
    #~ if not User.objects.filter( email = response['email'] ).exists():
        #~ search_user_error = True
    #~ 
    #~ return render_to_response( "base_pass_remind.html", { "page_title" : "Востановление пароля", "form" : form, "search_user_error" : _error }, context_instance = RequestContext( request ) )
