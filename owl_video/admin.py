# -*- coding: utf-8 -*-
from django.contrib import admin
from django.core.urlresolvers import reverse
from base_site.owl_video.models import Entry, Project, StorageInstance, Video, Event

# admin.site.register( Entry )
# admin.site.register( Event )
# admin.site.register( Project )
# admin.site.register( StorageInstance )
#admin.site.register( Video )

def show_owner(obj):
	return '<a href="{0}">{1}</a>'.format( reverse('admin:auth_user_change', args = [ obj.owner.id ]), obj.owner.email )
show_owner.short_description = 'owner'
show_owner.allow_tags = True

def show_creator(obj):
	return '<a href="{0}">{1}</a>'.format( reverse('admin:auth_user_change', args = [ obj.creator.id ]), obj.creator.email )
show_creator.short_description = 'creator'
show_creator.allow_tags = True


@admin.register(Entry)
class EntryAdmin( admin.ModelAdmin ):
	list_display = ( 
		'id', 'title', 'raw_title', show_owner,
		'restore_path', 'youtube_ui', 'net_link',
		'description', 'meta_data', 'add_date', 'modify_date',
		'hash_sha1', 'status', 'entry_type', 'size',
		'filemark' )
	list_display_links = ( 'id', 'title' )
	search_fields = [ 'id', 'title', '=restore_path', '=youtube_ui', '=net_link', 'owner__email', 'meta_data', '=hash_sha1' ]

@admin.register(Event)
class EventAdmin( admin.ModelAdmin ):
	list_display = (
		'id', 'name', show_creator
	)
	list_display_links = ( 'id', 'name' )
	search_fields = [ 'id', 'name', 'creator__email' ]

@admin.register(Project)
class ProjectAdmin( admin.ModelAdmin ):
	list_display = (
		'id', 'title', show_owner,
		'restore_path', 'youtube_ui', 'net_link',
		'add_date', 'modify_date', 'description', 'status'
	)
	list_display_links = ( 'id', 'title' )
	search_fields = [ 'id', 'title', 'owner__email', '=restore_path', '=youtube_ui', '=net_link' ]
	filter_horizontal = [ 'entrys' ]

@admin.register(StorageInstance)
class StorageInstanceAdmin( admin.ModelAdmin ):
	list_display = (
		'id',
		'name',
		'storage_identifier',
		show_owner,
		'add_date',
		'all_space',
		'free_space',
		'state',
		'storage_type'
	)
	list_display_links = ( 'id', 'name' )
	search_fields = [ 'id', 'name', '=storage_identifier', 'owner__email' ]
	filter_horizontal = [ 'entrys', 'projects' ]

@admin.register(Video)
class VideoAdmin( admin.ModelAdmin ):
	
	list_display = (
		'id', 'title', 'status',
		show_owner,
		'raw_title', 'youtube_ui',
		'add_date', 'hash_sha1'
	)

	list_display_links = ( 'id', 'title' )
	search_fields = [ 'id', 'title', 'owner__email' ]