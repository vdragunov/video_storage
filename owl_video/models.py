# -*- coding: utf-8 -*-
import pickle
from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from oauth2client.django_orm import FlowField, CredentialsField

class GoogleOauth2RTokenField( models.Field ):
    
    description = "Long-term storage for google-user refresh_token"
    
    __metaclass__ = models.SubfieldBase
    
    def __init__(self, *args, **kwargs):
        super(GoogleOauth2RTokenField, self).__init__(*args, **kwargs)
        
    def db_type(self, connection):
        return "LongText"
    
    #~ def to_python(self, value):
        #~ if value:
            #~ return pickle.loads( value )
        #~ else:
            #~ return None
            #~ 
    #~ def get_db_prep_value(self, value, connection, prepared=False):
        #~ if value:
            #~ return pickle.dumps( value )
        #~ else:
            #~ return u''
        

class Video( models.Model ):
    title = models.CharField( max_length = 255 ) #name.format
    raw_title = models.CharField( max_length = 255, null = True, blank = True ) #!!! sync db <db_id>.format
    owner =  models.ForeignKey( User )
    #view_url = models.URLField( max_length = 2000, null = True, blank = True )
    youtube_ui = models.CharField( max_length = 3000, null = True, blank = True )
    #raw_url = models.URLField(null = True, blank = True, max_length = 2000 )
    description = models.TextField( null = True, blank = True )
    add_date = models.DateTimeField( auto_now_add = True, editable=False, blank=True )
    hash_sha1 = models.CharField( max_length = 40, null = True, blank = True )
    status = models.CharField( max_length = 255 )
    
    def get_absolute_url(self):
        return reverse( 'base_site.owl_video.views.viewVideoEntry', args = [ str( self.id ) ] )
    
    def __unicode__(self):
        return u"{0} {1}".format( self.id, self.title )
    class Meta:
        permissions = ( ('view_video', 'View video'), )

class Entry( models.Model ):

    ENTRY_TYPES_CHOICES = ( ( 'VT', 'Video_Type' ), ( 'OT', 'Other_Type' ) )

    title = models.CharField( max_length = 255 ) #name.format
    raw_title = models.CharField( max_length = 255, null = True, blank = True )

    owner = models.ForeignKey( User )

    restore_path = models.CharField( max_length = 10000, null = True, blank = True )
    youtube_ui = models.CharField( max_length = 3000, null = True, blank = True )
    net_link = models.CharField( max_length = 10000, null = True, blank = True )
    
    description = models.TextField( null = True, blank = True )
    meta_data = models.TextField( null = True, blank = True )

    add_date = models.DateTimeField( auto_now_add = True, editable=False, blank=True )
    modify_date = models.DateTimeField( editable = False, null = True, blank=True )
    
    hash_sha1 = models.CharField( max_length = 40, null = True, blank = True )
    status = models.CharField( max_length = 255 )
    entry_type = models.CharField( max_length = 2, choices = ENTRY_TYPES_CHOICES, default = 'OT' )
    
    size = models.PositiveIntegerField()
    filemark = models.PositiveIntegerField( null = True, blank = True )
    
    def get_absolute_url(self):
        return reverse( 'base_site.owl_video.views.viewVideoEntry', args = [ str( self.id ) ] )
    
    def __unicode__(self):
        return u"{0} {1}".format( self.id, self.title )
    class Meta:
        permissions = ( ('view_entry', 'View entry'), )

class Project( models.Model ):

    title = models.CharField( max_length = 255 )

    entrys = models.ManyToManyField( Entry, null = True, blank = True )
    owner = models.ForeignKey( User )

    restore_path = models.CharField( max_length = 10000, null = True, blank = True )
    youtube_ui = models.CharField( max_length = 3000, null = True, blank = True )
    net_link = models.CharField( max_length = 10000, null = True, blank = True )

    add_date = models.DateTimeField( auto_now_add = True, editable=False, blank=True )
    modify_date = models.DateTimeField( editable = False, null = True, blank=True )
    description = models.TextField( null = True, blank = True )
    status = models.CharField( max_length = 255 )

    def __unicode__(self):
        return u"{0} {1}".format( self.id, self.title )

    class Meta:
        permissions = ( ('view_project', 'View project'), )

class StorageInstance( models.Model ):

    STATES_CHOICES = ( ( 'CF', 'Completely_Full' ), ( 'SE', 'Space_Exists' ), ( 'UN', 'Undefined' ) )
    TYPES_CHOICES = ( ( 'DD', 'Disk_Device' ), ( 'CD', 'Cartridge_Device' ), ( 'SD', 'Server_Device' ) )

    name = models.CharField( max_length = 255 )
    storage_identifier = models.CharField( max_length = 40 ) # sha1( owner@db_storage_id )

    projects = models.ManyToManyField( Project, null = True, blank = True )
    entrys = models.ManyToManyField( Entry, null = True, blank = True )
    owner = models.ForeignKey( User )

    add_date = models.DateTimeField( auto_now_add = True, editable=False, blank=True )
    description = models.TextField( null = True, blank = True )
    all_space = models.BigIntegerField( null = True, blank = True )
    free_space = models.BigIntegerField( null = True, blank = True )

    state = models.CharField( max_length = 2, choices = STATES_CHOICES, default = 'UN' )
    storage_type = models.CharField( max_length = 2, choices = TYPES_CHOICES, default = 'DD' )

    def __unicode__(self):
        return u"{0} {1}".format( self.id, self.name )

    class Meta:
        permissions = ( ('view_storageinstance', 'View storageinstance'), )

class Event( models.Model ):
    name = models.CharField( max_length = 255, unique = True )
    description = models.TextField( null = True, blank = True )
    #videos = models.ManyToManyField( Video )
    creator = models.ForeignKey( User )
    entrys = models.ManyToManyField( Entry )
    projects = models.ManyToManyField( Project )
    
    def get_absolute_url(self):
        return reverse( 'base_site.owl_video.views.viewEventEntry', args = [ str( self.id ) ] )
    
    def __unicode__(self):
        return u"{0} {1}".format( self.id, self.name )
    class Meta:
        permissions = ( ('view_event', 'View event'), )

#class Account( models.Model ):
#    name_of_type = models.CharField( max_length = 255 )
#    users = models.ManyToManyField( User )
#    
#    def __unicode__(self):
#        return u"{0} {1}".format( self.id, self.name_of_type )
    
class Oauth2Credential( models.Model ):
  id = models.ForeignKey( User, primary_key=True, db_column='id' )
  credential = CredentialsField()
  rtoken = GoogleOauth2RTokenField(null = True)

